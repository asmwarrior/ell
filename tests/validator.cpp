#include "test.h"
#include <ell/Grammar.h>

struct FibonacciChecker: public ell::Grammar
{
    unsigned a = 0;
    unsigned b = 0;

    FibonacciChecker()
    {
        root = eps.action([&]{ a=b=0; }) >>
            skip(ch(' '),
                udec[b].action([&]{return b==1;})
                >> *(udec.action<unsigned>([&](unsigned c){
                    unsigned sum = a + b;
                    a = b;
                    b = c;
                    return sum == c;
                    })))
                >> eos;
    }

    ell::Rule root;
};

int main(int argc, const char ** argv)
{
    FibonacciChecker f;
    ell::Parser p;
    p.showTrace();

    ELL_TEST(ELL_PARSE(p, f.root, "1 1 2 3 5 8 13"));
    ELL_TEST(ELL_PARSE(p, f.root, "1 1"));
    ELL_TEST(ELL_PARSE(p, f.root, "1"));
    ELL_TEST_ERROR(ELL_PARSE(p, f.root, "1 1 2 3 6 8 13"));
    return 0;
}
