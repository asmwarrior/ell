#include <functional>
#include <string>
#include <cstring>

#define ELL_TEST(code) do{ code; }while(false)
#define ELL_TEST_EQUAL(v1, v2) do{if ((v1) != (v2)) throw ell::Error(#v1 "!=" #v2);}while(false)
#define ELL_TEST_ERROR(code) do{ bool ok = true; try{ code; } catch(ell::Error &) { ok = false; } if (ok) throw ell::Error(#code " should have raised an error"); } while (false)

#define ELL_PARSE(parser, root, str) parser.match(root, "", str, std::strlen(str))
