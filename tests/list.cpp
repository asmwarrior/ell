#include "test.h"
#include "ell/Grammar.h"

struct ListGrammar : public ell::Grammar
{
    ell::Rule root = skip(ch(' '), (dec % ch(',')) >> eos);
};

int main(int argc, const char ** argv)
{
    ListGrammar lg;
    ell::Parser p;
    p.showTrace();

    ELL_TEST(ELL_PARSE(p, lg.root, "123"));
    ELL_TEST(ELL_PARSE(p, lg.root, "1,2,3"));
    ELL_TEST(ELL_PARSE(p, lg.root, "11 ,2, 3"));
    ELL_TEST_ERROR(ELL_PARSE(p, lg.root, "1,2,"));
    return 0;
}
