#pragma once
#include "NodeChild.h"
#include <functional>
#include <vector>
#include <cstring>
namespace ell
{
    // Abstract class for grammar unary nodes
    template <typename Child, typename ConcreteNode>
    struct UnaryNodeBase : public ConcreteNodeBase<ConcreteNode>
    {
    protected:
        UnaryNodeBase(const ConcreteNodeBase<Child> & child)
          : child(child.cast())
        { }

        NodeChild<Child> child;
    };

    // Implementation of + * ! BNF operators
    template <const int Min, const int Max, typename Child>
    struct RepeatNode final : public UnaryNodeBase<Child, RepeatNode<Min,Max,Child>>
    {
        using ConcreteNodeBase<RepeatNode<Min,Max,Child>>::match;

        RepeatNode(const ConcreteNodeBase<Child> & child)
          : UnaryNodeBase<Child,RepeatNode<Min,Max,Child>>(child)
        { }

        void describe(std::ostream & os) const override
        {
            if (Min == 0 && Max == 1)
                os << '!';
            else if (Min == 0 && Max == -1)
                os << '*';
            else if (Min == 1 && Max == -1)
                os << '+';
            else 
            {
                os << this->child << '{' << Min << ',' << Max << '}';
                return;
            }

            os << '(' << this->child << ')';
        } 

        bool match(Parser * p) const
        {
            p->traceBegin(this);

            Location begin = p->location();
            Location beforeSkipping = begin;
            int count = 0;
            while ((Max == -1 || count < Max) && this->child.node.match(p))
            {
                ++count;
                beforeSkipping = p->location();
                p->trySkip();
            }

            if (count >= Min)
            {
                p->setLocation(beforeSkipping);
                p->traceEnd(this, "=");
                return true;
            }

            p->setLocation(begin);
            p->traceEnd(this, "#");
            return false;
        }
    };

    // Repetition of a node a particular number of times which is given by a variable
    template <typename Child, typename IntType>
    struct DynRepeatNode final : public UnaryNodeBase<Child, DynRepeatNode<Child,IntType>>
    {
        using ConcreteNodeBase<DynRepeatNode<Child,IntType>>::match;

        DynRepeatNode(const ConcreteNodeBase<Child> & child, const IntType & times)
          : UnaryNodeBase<Child,DynRepeatNode<Child,IntType>>(child),
            _times(times)
        { }

        void describe(std::ostream & os) const override { os << '(' << this->child << "){" << _times << '}'; }

        bool match(Parser * p) const
        {
            p->traceBegin(this);
            Location begin = p->location();
            Location beforeSkipping = begin;
            IntType count = 0;
            IntType times = _times;
            while (count < times && this->child.node.match(p))
            {
                ++count;
                beforeSkipping = p->location();
                p->trySkip();
            }

            if (count == times)
            {
                p->setLocation(beforeSkipping);
                p->traceEnd(this, true);
                return true;
            }

            p->setLocation(begin);
            p->traceEnd(this, false);
            return false;
        }

    private:
        const IntType & _times;
    };

    template <typename Child>
    const RepeatNode<0,-1,Child> operator * (const ConcreteNodeBase<Child> & arg) { return RepeatNode<0,-1,Child>(arg); }

    template <typename Child>
    const RepeatNode<1,-1,Child> operator + (const ConcreteNodeBase<Child> & arg) { return RepeatNode<1,-1,Child>(arg); }

    template <typename Child>
    const RepeatNode<0,1,Child> operator ! (const ConcreteNodeBase<Child> & arg) { return RepeatNode<0,1,Child>(arg); }

    template <typename Child, typename IntType>
    const DynRepeatNode<Child, IntType> operator * (const ConcreteNodeBase<Child> & arg, IntType & times) { return DynRepeatNode<Child, IntType>(arg, times); }

    template <typename Child, typename IntType>
    const DynRepeatNode<Child,IntType> operator * (const IntType & times, const ConcreteNodeBase<Child> & arg) { return DynRepeatNode<Child,IntType>(arg, times); }

    // Semantic action operator
    template <typename Lambda, typename Var, typename Child>
    struct ActionNode final : public UnaryNodeBase<Child, ActionNode<Lambda,Var,Child>>
    {
        using ConcreteNodeBase<ActionNode<Lambda,Var,Child>>::match;

        ActionNode(const ConcreteNodeBase<Child> & node, Lambda f)
          : UnaryNodeBase<Child, ActionNode<Lambda,Var,Child>>(node),
            _func(f)
        { }

        void dump(std::ostream & os) const override { os << "action(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }

        bool match(Parser * p, Var * v) const
        {
            if constexpr (std::is_same_v<bool,decltype(std::declval<Lambda>()(*v))>)
            {
                Location begin = p->location();
                if (! this->child.node.match(p, v))
                    return false;
                if (_func(*v))
                    return true;
                p->setLocation(begin);
                return false;
            }
            else if (this->child.node.match(p, v))
            {
                _func(*v);
                return true;
            }
            else return false;
        }

        bool match(Parser * p) const
        {
            Var v = Var();
            return match(p, & v);
        }

    private:
        Lambda _func;
    };

    // Specialization for an action which does not take any parameter.
    template <typename Lambda, typename Child>
    struct ActionNode<Lambda,void,Child> final : public UnaryNodeBase<Child, ActionNode<Lambda,void,Child>>
    {
        using ConcreteNodeBase<ActionNode<Lambda,void,Child>>::match;

        ActionNode(const ConcreteNodeBase<Child> & node, Lambda func)
          : UnaryNodeBase<Child, ActionNode<Lambda,void,Child>>(node),
            _func(func)
        { }

        void dump(std::ostream & os) const override { os << "action(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }

        bool match(Parser * p) const
        {
            if constexpr (std::is_same_v<bool,decltype(std::declval<Lambda>()())>)
            {
                Location begin = p->location();
                if (! this->child.node.match(p))
                    return false;
                if (_func())
                    return true;
                p->setLocation(begin);
                return false;
            }
            else if (this->child.node.match(p))
            {
                _func();
                return true;
            }
            else return false;
        }

    private:
        Lambda _func;
    };

    // Implementation of ConcreteNode::action now that ActionNode is defined
    template <typename ConcreteNode>
    template <typename Var, typename Lambda>
    const ActionNode<Lambda,Var,ConcreteNode> ConcreteNodeBase<ConcreteNode>::action(Lambda func) const
    {
        return ActionNode<Lambda, Var, ConcreteNode>(*this, func);
    }

    template <typename Var, typename Child>
    struct AssignNode final : public UnaryNodeBase<Child,AssignNode<Var,Child>>
    {
        using ConcreteNodeBase<AssignNode<Var,Child>>::match;

        AssignNode(const ConcreteNodeBase<Child> & node, Var & var)
          : UnaryNodeBase<Child, AssignNode<Var,Child>>(node),
            _var(var)
        { }

        void dump(std::ostream & os) const override { os << "assign(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }

        bool match(Parser * p, Var * v) const
        {
            bool r = this->child.node.match(p, v);
            if (r)
                _var = *v;
            return r;
        }

        bool match(Parser * p) const { return match(p, & _var); }

    private:
        Var & _var;
    };

    template <typename ConcreteNode>
    template <typename Var>
    const AssignNode<Var,ConcreteNode> ConcreteNodeBase<ConcreteNode>::operator [] (Var & var) const
    {
        return AssignNode<Var, ConcreteNode>(*this, var);
    }

    // Skipper disabling
    template <typename Child>
    struct NoSkipNode final : public UnaryNodeBase<Child,NoSkipNode<Child>>
    {
        using ConcreteNodeBase<NoSkipNode<Child>>::match;

        NoSkipNode(const ConcreteNodeBase<Child> & child)
          : UnaryNodeBase<Child,NoSkipNode<Child>>(child)
        { }

        void dump(std::ostream & os) const override { os << "noskip(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }

        bool match(Parser * p) const
        {
            SafeModify<const NodeBase*> ms(p->_skipper, nullptr);
#if ELL_DEBUG == 1
            // Disable trace inside terminals
            SafeModify<bool> mt(p->_showTrace, false);
#endif
            return this->child.node.match(p);
        }
    };

    // Disables error when second operand of aggregation does not match
    template <typename Child>
    struct FallbackNode final : public UnaryNodeBase<Child,FallbackNode<Child>>
    {
        using ConcreteNodeBase<FallbackNode<Child>>::match;

        FallbackNode(const ConcreteNodeBase<Child> & child)
          : UnaryNodeBase<Child,FallbackNode<Child>>(child)
        { }

        void dump(std::ostream & os) const override { os << "fallback(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }

        bool match(Parser * p) const
        {
            p->traceBegin(this);
            SafeModify<bool> ms(p->_fallback, true);
            return p->traceEnd(this, this->child.node.match(p));
        }
    };

    // Match without consuming any character
    template <typename Child>
    struct NoConsumeNode final : public UnaryNodeBase<Child,NoConsumeNode<Child>>
    {
        using ConcreteNodeBase<NoConsumeNode<Child>>::match;

        NoConsumeNode(const ConcreteNodeBase<Child> & child)
          : UnaryNodeBase<Child,NoConsumeNode<Child>>(child)
        { }

        void describe(std::ostream & os) const override { os << "check(" << this->child << ")"; }

        bool match(Parser * p) const
        {
            p->traceBegin(this);
            Location begin = p->location();
            bool r = this->child.node.match(p);
            p->setLocation(begin);
            return p->traceEnd(this, r);
        }
    };
}
