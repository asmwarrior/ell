#pragma once
#include "ConcreteNodeBase.h"
#include "Utils.h"
#include <charconv>
#include <vector>
#include <initializer_list>
namespace ell
{
    // Set of characters, use minus to specify a range (ex: -a-zA-Z_0-9)
    struct CharSetNode final : public ConcreteNodeBase<CharSetNode>
    {
        using ConcreteNodeBase<CharSetNode>::match;

        CharSetNode(const char * set)
          : _set(set)
        { }

        bool match(Parser * p) const override
        {
            char c;
            return match(p, & c);
        }

        bool match(Parser * parser, char * out) const
        {
            const char * p = & _set[0];
            const char c = *parser->position();
            while (* p)
            {
                const char first = * p;
                if (c == first)
                {
                    *out = c;
                    parser->next();
                    parser->trace(this, "=");
                    return true;
                }
                ++p;
                if (* p == '-' && * (p + 1))
                {
                    const char second = * (p + 1);

                    if ((c >= first) & (c <= second))
                    {
                        *out = c;
                        parser->next();
                        parser->trace(this, "=");
                        return true;
                    }

                    p += 2;
                }
            }
            parser->trace(this, "#");
            return false;
        }

        void describe(std::ostream & os) const override
        {
            os << '[';
            dump_string(os, _set);
            os << ']';
        }

    private:
        const char* _set;
    };

    // Match a particular character
    struct CharNode final : public ConcreteNodeBase<CharNode>
    {
        using ConcreteNodeBase<CharNode>::match;

        CharNode(char c)
          : _c(c)
        { }

        bool match(Parser * p) const override
        {
            char c;
            return match(p, & c);
        }

        bool match(Parser * parser, char * out) const
        {
            if (* parser->position() == _c)
            {
                *out = _c;
                parser->next();
                parser->trace(this, "=");
                return true;
            }
            parser->trace(this, "#");
            return false;
        }

        void describe(std::ostream & os) const override
        {
            os << '\'';
            dump_char(os, _c);
            os << '\'';
        }

    private:
        char _c;
    };

    // Match an inclusive range of characters
    struct RangeNode final : public ConcreteNodeBase<RangeNode>
    {
        using ConcreteNodeBase<RangeNode>::match;

        RangeNode(char from, char to)
          : _from(from), _to(to)
        { }

        bool match(Parser * p) const override
        {
            char c;
            return match(p, & c);
        }

        bool match(Parser * p, char * out) const
        {
            const char c = * p->position();
            if (c >= _from && c <= _to)
            {
                *out = c;
                p->next();
                p->trace(this, "=");
                return true;
            }
            p->trace(this, "#");
            return false;
        }

        void describe(std::ostream & os) const override
        {
            os << '[';
            dump_char(os, _from);
            os << '-';
            dump_char(os, _to);
            os << ']';
        }

    private:
        char _from, _to;
    };

    // Matches a string
    struct StrNode final : public ConcreteNodeBase<StrNode>
    {
        using ConcreteNodeBase<StrNode>::match;

        StrNode(const char * s)
          : _string(s)
        { 
            if constexpr (ELL_DEBUG == 1)
            {
                if (_string.find('\n') != _string.npos)
                    throw Error("New line character in string primitive not supported");
            }
        }

        bool match(Parser * parser) const override
        {
            if (parser->remaining() < (int)_string.size() ||
                memcmp(_string.c_str(), parser->position(), _string.size()) != 0)
            {
                parser->trace(this, "#");
                return false;
            }

            parser->sameLineNext(_string.size());
            parser->trace(this, "=");
            return true;
        }

        void describe(std::ostream & os) const override
        {
            os << '"';
            dump_string(os, _string);
            os << '"';
        }

    private:
        std::string _string;
    };

    // Matches a string, ignoring case
    struct IgnStrNode final : public ConcreteNodeBase<IgnStrNode>
    {
        using ConcreteNodeBase<IgnStrNode>::match;

        static char to_lower(char c) { return ((c >= 'A') & (c <= 'Z')) ? (c - 'A' + 'a') : c; }

        IgnStrNode(const char * s)
          : _string(s)
        {
            if constexpr (ELL_DEBUG == 1)
            {
                if (_string.find('\n') != _string.npos)
                    throw Error("New line character in string primitive not supported");
            }
            std::transform(_string.begin(), _string.end(), _string.begin(), to_lower);
        }

        bool match(Parser * parser) const override
        {
            int n = _string.size();
            if (parser->remaining() < n)
            {
                parser->trace(this, "#");
                return false;
            }

            Location begin = parser->location();
            for (int i = 0; i < n; ++i)
            {
                if (to_lower(parser->position()[i]) != _string[i])
                {
                    parser->setLocation(begin);
                    parser->trace(this, "#");
                    return false;
                }
            }
            parser->sameLineNext(n);
            parser->trace(this, "=");
            return true;
        }

        void describe(std::ostream & os) const override
        {
            os << "i\"";
            dump_string(os, _string);
            os << '"';
        }

    private:
        std::string _string;
    };

    // Matches end of the string being parsed
    struct EosNode final : public ConcreteNodeBase<EosNode>
    {
        using ConcreteNodeBase<EosNode>::match;

        void describe(std::ostream & os) const override { os << "end"; }

        bool match(Parser * p) const override
        {
            bool matched = p->remaining() <= 0;
            p->trace(this, matched ? "=" : "#");
            return matched;
        }
    };

    // Matches nothing, usefull to apply a default semantic action on an branch
    struct EpsNode final : public ConcreteNodeBase<EpsNode>
    {
        using ConcreteNodeBase<EpsNode>::match;

        void describe(std::ostream & os) const override { os << "eps"; }

        bool match(Parser * p) const override
        {
            p->trace(this, "=");
            return true;
        }
    };

    // Matches any character
    struct AnyNode final : public ConcreteNodeBase<AnyNode>
    {
        using ConcreteNodeBase<AnyNode>::match;

        void describe(std::ostream & os) const override { os << "any"; }

        bool match(Parser * p) const override
        {
            char c;
            return match(p, & c);
        }

        bool match(Parser * p, char * out) const
        {
            if (p->remaining() == 0)
            {
                p->trace(this, "#");
                return false;
            }
            * out = * p->position();
            p->next();
            p->trace(this, "=");
            return true;
        }
    };

    // Never matches
    struct NopeNode final : public ConcreteNodeBase<NopeNode>
    {
        using ConcreteNodeBase<NopeNode>::match;

        void describe(std::ostream & os) const override { os << "nop"; }

        bool match(Parser * p) const override
        {
            p->trace(this, "#");
            return false;
        }
    };

    // Matches a real constant
    struct RealNode final : public ConcreteNodeBase<RealNode>
    {
        using ConcreteNodeBase<RealNode>::match;

        void describe(std::ostream & os) const override { os << "real"; }

        bool match(Parser * p) const override
        {
            double d;
            return match(p, & d);
        }

        bool match(Parser * p, double * out) const
        {
            char * end;
            // from_chars not suported for double at the moment
            // end = std::from_chars(p->position(), p->end(), *out).ptr;
            * out = strtod(p->position(), & end);
            if (end == p->position())
            {
                p->trace(this, "#");
                return false;
            }
            p->sameLineNext(end - p->position());
            p->trace(this, "=");
            return true;
        }
    };

    // Matches an integer constant
    template <int Base=10, int MinChars=1, int MaxChars=-1, typename IntType=int>
    struct IntegerNode final : public ConcreteNodeBase<IntegerNode<Base,MinChars,MaxChars,IntType>>
    {
        using ConcreteNodeBase<IntegerNode<Base,MinChars,MaxChars,IntType>>::match;

        void describe(std::ostream & os) const override
        {
            if constexpr (Base == 16)
                os << "hexadecimal";
            else if constexpr (Base == 8)
                os << "octal";
            else
                os << "integer";
        }

        bool match(Parser * p) const override
        {
            IntType d;
            return match(p, & d);
        }

        bool match(Parser * p, IntType * out) const
        {
            const char * end;
            if constexpr (MaxChars == -1)
                end = p->end();
            else
                end = std::min(p->position() + MaxChars, p->end());
            end = std::from_chars(p->position(), end, *out, Base).ptr;
            int consumed = end - p->position();

            if (consumed < MinChars)
            {
                p->trace(this, "#");
                return false;
            }
            p->sameLineNext(consumed);
            p->trace(this, "=");
            return true;
        }
    };

    // Raise an error with custom message
    struct ErrorNode final : public ConcreteNodeBase<ErrorNode>
    {
        using ConcreteNodeBase<ErrorNode>::match;

        ErrorNode(const std::string & msg)
          : _msg(msg)
        { }

        bool match(Parser * p) const override
        {
            p->raise_error(nullptr, _msg.c_str());
            return false;
        }

        void describe(std::ostream & os) const override { os << "error(\"" << _msg << "\")"; }

    private:
        std::string _msg;
    };
}

