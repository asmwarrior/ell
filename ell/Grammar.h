#pragma once
#include "Parser.h"
#include "Rule.h"
#include "BinaryNodes.h"
#include "UnaryNodes.h"
#include "PrimitiveNodes.h"

namespace ell
{
    // Any ell grammar should extends this class.
    struct Grammar
    {
        // Binary operators
        // (also see operators `>>`, `|` and `-`.)
        template <typename Left, typename Right> using nosfx = NoSuffixNode<Left, Right>;
        template <typename Left, typename Right> using skip = SkipNode<Left, Right>;

        // Unary operators
        // (also see operators `+`, `*`, `!`, `[]` operators and `action` method.)
        template <typename Child> using noskip = NoSkipNode<Child>;
        template <typename Child> using fallback = FallbackNode<Child>;
        template <typename Child> using check = NoConsumeNode<Child>;

        template <int Min, int Max, typename Child>
        const RepeatNode<Min, Max, Child> repeat(const ConcreteNodeBase<Child> & arg) { return RepeatNode<Min, Max, Child>(arg); }

        // Short-hand to express a lexeme in the grammar (ie. making a new primitive)
        template <typename Child>
        const FallbackNode<NoSkipNode<Child>> token(const ConcreteNodeBase<Child> & child)
        {
            return fallback(noskip(child));
        }

        // Primitives
        using chset = CharSetNode;
        using ch = CharNode;
        using range = RangeNode;
        using str = StrNode;
        using istr = IgnStrNode;
        using error = ErrorNode;

        template <int Base=10, int MinChars=1, int MaxChars=-1, typename IntType=int>
        using integer = IntegerNode<Base,MinChars,MaxChars,IntType>;

        const IntegerNode<> dec;
        const IntegerNode<10,1,-1,unsigned int> udec;
        const RealNode real;
        const EosNode eos;
        const EpsNode eps;
        const NopeNode nop;
        const AnyNode any;
    };
}
