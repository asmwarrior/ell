#pragma once
#include "ConcreteNodeBase.h"
namespace ell
{
    struct Rule final : public ConcreteNodeBase<Rule>
    {
        using ConcreteNodeBase<Rule>::match;

        Rule(const char * name = nullptr)
          : name(name), _value(nullptr)
        { }

        template <typename ConcreteNode>
        Rule(const ConcreteNodeBase<ConcreteNode> & n)
          : name(nullptr), _value(nullptr)
        {
            operator=(n);
        }

        ~Rule() { free(); }

        void free()
        {
            // Only delete value if was allocated during assign
            auto r = dynamic_cast<const Rule *>(_value);
            if (r == nullptr)
                delete _value;
            _value = nullptr;
        }

        bool match(Parser * p) const override
        {
            if constexpr (ELL_DEBUG == 1)
            {
                if (this->name != nullptr)
                {
                    p->traceBegin(this);
                    return p->traceEnd(this, _value->match(p));
                }
            }

            return _value->match(p);
        }

        void describe(std::ostream & os) const override
        {
            if (this->name != nullptr)
            {
                os << this->name;
                return;
            }

            if (recursionFlag)
            {
                os << "...";
                return;
            }

            recursionFlag = true;
            _value->describe(os);
            recursionFlag = false;
        }

        template <typename ConcreteNode>
        void operator = (const ConcreteNodeBase<ConcreteNode> & n)
        {
            free(); // Why not reassigning a rule?
            auto r = dynamic_cast<const Rule *>(& n);
            if (r != nullptr)
            {
                _value = r;
                return;
            }

            _value = new ConcreteNode(n.cast());
        }

        Rule & setName(const char * name)
        { 
            this->name = name;
            return * this;
        }

        Rule & setTraceName(const char * name)
        { 
            if constexpr (ELL_DEBUG == 1)
                this->name = name;
            else
                this->name = nullptr;
            return * this;
        }

    private:
        const char * name;
        const NodeBase * _value;
        mutable bool recursionFlag = false;

        Rule(const Rule &);
        void operator = (const Rule &);
    };
}

