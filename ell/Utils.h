#pragma once
#include "NodeBase.h"
#include <iomanip>
#include <sstream>
#include <initializer_list>
namespace ell
{
    // Exception-safe temporary modification of flag
    template <typename T>
    struct SafeModify
    {
        SafeModify(T & _var, T value)
            : var(_var),
              sav(_var)
        {
            var = value;
        }

        ~SafeModify() { var = sav; }

        T & var;
        T sav;
    };

    // Escape a character to make it readable
    inline void dump_char(std::ostream & os, char c)
    {
        if (c == 0)
            os << "\\0";
        else if ((c >= '\a') & (c <= '\r'))
            os << '\\' << "abtnvfr"[c - '\a'];
        else if ((c == '"') | (c == '\\'))
            os << '\\' << c;
        else if (((c > 0) & (c < ' ')) | ((c & 0xFFFFFF80) != 0))
            os << "\\<" << std::hex << (int)(unsigned char)c << std::dec << ">";
        else
            os << c;
    }

    inline void dump_string(std::ostream & os, const std::string & s)
    {
        for (char c : s)
            dump_char(os, c);
    }

}
