#pragma once
#include "NodeBase.h"
#include "Parser.h"
#include <functional>
namespace ell
{
    template <typename Lambda, typename Var, typename Child>
    struct ActionNode;

    template <typename Var, typename Child>
    struct AssignNode;

    template<typename ConcreteNode>
    struct ConcreteNodeBase : public NodeBase
    {
        using NodeBase::match;

        template <typename Var=void, typename Lambda>
        const ActionNode<Lambda,Var,ConcreteNode> action(Lambda func) const;

        template <typename Var>
        const AssignNode<Var,ConcreteNode> operator [] (Var & var) const;

        bool match(Parser * p, const char **begin) const
        {
            *begin = p->position();
            return cast().match(p);
        }

        bool match(Parser * p, std::string * s) const
        {
            const char * begin = p->position();
            bool r = cast().match(p);
            if (r)
            {
                s->assign(begin, p->position());
            }
            return r;
        }

        bool match(Parser * p, Location * begin) const
        {
            * begin = p->location();
            return cast().match(p);
        }

        const ConcreteNode & cast() const { return static_cast<const ConcreteNode &>(* this); }
    };
}
