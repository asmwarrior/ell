#pragma once
#include <string>
#include <iostream>
namespace ell
{
    struct Parser;

    struct NodeBase
    {
        virtual ~NodeBase() { }

        // Function called recursively to perform the parsing.
        // Returns true if and only if it matches.
        // Each node is responsible for restoring the parser state as untouched unless it has matched.
        virtual bool match(Parser *) const = 0;

        // Displaid in trace
        virtual void dump(std::ostream & os) const { describe(os); }

        // Describe this node in error messages
        virtual void describe(std::ostream & os) const = 0;
    };
}
