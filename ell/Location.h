#pragma once
#include <iostream>
#include <cstring>
namespace ell
{
    struct Location
    {
        const char * filename;
        const char * position;
        int line;
        int column;
        Location * parent;

        friend std::ostream & operator << (std::ostream & os, const Location & l)
        {
           os << l.filename << ":" << l.line << ":" << l.column;
           return os;
        }

        bool operator < (const Location & l) const
        {
            if (filename != l.filename)
            {
                if (l.parent == nullptr)
                    return false;

                if (parent == nullptr)
                    return true;

                return * parent < * l.parent;
            }

            return position < l.position;
        }

        friend bool operator > (const Location & l1, const Location &l2) { return l2 < l1; }
        friend bool operator == (const Location & l1, const Location &l2) { return ! (l2 != l1); }
        friend bool operator != (const Location & l1, const Location &l2) { return l1 < l2 || l1 > l2; }
    };
}
