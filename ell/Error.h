#pragma once
#include <sstream>
#include <exception>
namespace ell
{
    struct Error : public std::exception
    {
        explicit Error(const std::string & s)
        {
           _msg = s;
        }

        const char* what() const throw()
        {
            return _msg.c_str();
        }

    protected:
        std::string _msg;
    };
}
