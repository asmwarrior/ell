/*
Ell library, version 2.0

A library to Embed LL grammar & parser in C++ code.

https://gitlab.com/samuel.hangouet/ell

Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2005-2021 Samuel Hangouët

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#pragma once
#include <charconv>
#include <cstring>
#include <exception>
#include <functional>
#include <initializer_list>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#ifndef ELL_DEBUG
#define ELL_DEBUG 1
#endif
namespace ell
{
    struct Error : public std::exception
    {
        explicit Error(const std::string & s)
        {
           _msg = s;
        }
        const char* what() const throw()
        {
            return _msg.c_str();
        }
    protected:
        std::string _msg;
    };
    // Exception-safe temporary modification of flag
    template <typename T>
    struct SafeModify
    {
        SafeModify(T & _var, T value)
            : var(_var),
              sav(_var)
        {
            var = value;
        }
        ~SafeModify() { var = sav; }
        T & var;
        T sav;
    };
    // Escape a character to make it readable
    inline void dump_char(std::ostream & os, char c)
    {
        if (c == 0)
            os << "\\0";
        else if ((c >= '\a') & (c <= '\r'))
            os << '\\' << "abtnvfr"[c - '\a'];
        else if ((c == '"') | (c == '\\'))
            os << '\\' << c;
        else if (((c > 0) & (c < ' ')) | ((c & 0xFFFFFF80) != 0))
            os << "\\<" << std::hex << (int)(unsigned char)c << std::dec << ">";
        else
            os << c;
    }
    inline void dump_string(std::ostream & os, const std::string & s)
    {
        for (char c : s)
            dump_char(os, c);
    }
    struct Location
    {
        const char * filename;
        const char * position;
        int line;
        int column;
        Location * parent;
        friend std::ostream & operator << (std::ostream & os, const Location & l)
        {
           os << l.filename << ":" << l.line << ":" << l.column;
           return os;
        }
        bool operator < (const Location & l) const
        {
            if (filename != l.filename)
            {
                if (l.parent == nullptr)
                    return false;
                if (parent == nullptr)
                    return true;
                return * parent < * l.parent;
            }
            return position < l.position;
        }
        friend bool operator > (const Location & l1, const Location &l2) { return l2 < l1; }
        friend bool operator == (const Location & l1, const Location &l2) { return ! (l2 != l1); }
        friend bool operator != (const Location & l1, const Location &l2) { return l1 < l2 || l1 > l2; }
    };
    struct Parser;
    struct NodeBase
    {
        virtual ~NodeBase() { }
        // Function called recursively to perform the parsing.
        // Returns true if and only if it matches.
        // Each node is responsible for restoring the parser state as untouched unless it has matched.
        virtual bool match(Parser *) const = 0;
        // Displaid in trace
        virtual void dump(std::ostream & os) const { describe(os); }
        // Describe this node in error messages
        virtual void describe(std::ostream & os) const = 0;
    };
    // Every parser extends this class
    struct Parser
    {
        virtual ~Parser() { }
        void match(const NodeBase & root, const char * filename,
                   const char * content, size_t length,
                   int line = 1, int column = 1,
                   Location * parent = nullptr)
        {
            _level = 0;
            _location = { filename, content, line, column, parent };
            _begin = content;
            _end = content + length;
            if (! root.match(this))
                raise_error(&root);
        }
        // Returns current location
        const Location & location() const { return _location; }
        
        // Sets location
        void setLocation(const Location & location) { _location = location; }
        // If build with ELL_DEBUG==1, you can set this flag to show or hide the trace
        void showTrace(bool show = true) { _showTrace = show; }
        // Returns the number of remaining characters to parse
        int remaining() const { return _end - position(); }
        // Returns the current position in buffer being parsed
        const char * position() const { return _location.position; }
        // Returns pointer after the last character to parse
        const char * end() const { return _end; }
        // Consumes a character from parsed buffer and update current location
        void next()
        {
            if (* position() == '\n')
            {
                _location.column = 1;
                _location.line++;
            }
            else
                _location.column++;
            _location.position++;
        }
        // Consumes n characters
        void next(int n)
        {
            for (int i = 0; i < n; ++i)
                next();
        }
        // Same but faster than next() when the next n characters cannot be a newline
        void sameLineNext(int n)
        {
            _location.column += n;
            _location.position += n;
        }
        // Throws an parsing error using given node and current buffer position to build the message
        void raise_error(const NodeBase * mismatch, const char * message=nullptr) const
        {
            std::ostringstream os;
            os << _location << ": error: ";
            if (mismatch != nullptr)
            {
                os << "expecting ";
                mismatch->describe(os);
                os << " before ";
                dumpPosition(os);
            }
            if (message != nullptr)
            {
                if (mismatch == nullptr)
                {
                    os << "at ";
                    dumpPosition(os);
                }
                os << ": " << message;
            }
            throw Error(os.str());
        }
        // If a skipper has been set, tries to advance among buffer matching it
        void trySkip()
        {
            if (_skipper == nullptr)
                return;
            const NodeBase * skipper = _skipper;
            SafeModify<const NodeBase*> ms(_skipper, nullptr);
            SafeModify<bool> mt(_showTrace, false);
            while (skipper->match(this));
        }
        // Dumps current position of buffer being parsed
        void dumpPosition(std::ostream & os) const
        {
            if (remaining() == 0)
            {
                os << "end";
                return;
            }
            os << '"';
            for(int i = 0; i < remaining(); ++i)
            {
                char c = * (position() + i);
                ell::dump_char(os, c);
                if (c == '\n')
                    // Do not cross end of line
                    break;
                if (i == 19)
                {
                    os << "\"...";
                    return;
                }
            }
            os << '"';
        }
        void traceBegin(const NodeBase * node)
        {
            if constexpr (ELL_DEBUG == 1)
            {
                trace(node, "\\");
                _level++;
            }
        }
        bool traceEnd(const NodeBase * node, bool match)
        {
            if constexpr (ELL_DEBUG == 1)
            {
                _level--;
                trace(node, match ? "/" : "#");
            }
            return match;
        }
        // Can also be called from a semantic action
        void trace(const NodeBase * node = nullptr, const char * flag = nullptr, const char * msg = nullptr)
        {
            if constexpr (ELL_DEBUG == 1)
            {
                if (! _showTrace)
                    return;
                traceIndent();
                if (flag != nullptr)
                    std::cerr << flag << ' ';
                if (msg != nullptr)
                    std::cerr << msg << ' ';
                if (node != nullptr)
                {
                    node->dump(std::cerr);
                    std::cerr << ' ';
                }
                std::cerr << '\t';
                dumpPosition(std::cerr);
                std::cerr << std::endl;
            }
        }
        // Dumps white spaces according to current indentation level of trace
        void traceIndent()
        {
            if constexpr (ELL_DEBUG == 1)
                std::cerr << std::string(_level, ' ');
        }
    private:
        bool _showTrace = false;
        int _level = 0;
        const char * _begin = nullptr;
        const char * _end = nullptr;
        Location _location = {};
        const NodeBase * _skipper = nullptr;
        template <typename Left,typename Right> friend struct SkipNode;
        template <typename Child> friend struct NoSkipNode;
        bool _fallback = false;
        template <typename Child> friend struct FallbackNode;
        template <typename Left,typename Right> friend struct SequenceNode;
    };
    template <typename Lambda, typename Var, typename Child>
    struct ActionNode;
    template <typename Var, typename Child>
    struct AssignNode;
    template<typename ConcreteNode>
    struct ConcreteNodeBase : public NodeBase
    {
        using NodeBase::match;
        template <typename Var=void, typename Lambda>
        const ActionNode<Lambda,Var,ConcreteNode> action(Lambda func) const;
        template <typename Var>
        const AssignNode<Var,ConcreteNode> operator [] (Var & var) const;
        bool match(Parser * p, const char **begin) const
        {
            *begin = p->position();
            return cast().match(p);
        }
        bool match(Parser * p, std::string * s) const
        {
            const char * begin = p->position();
            bool r = cast().match(p);
            if (r)
            {
                s->assign(begin, p->position());
            }
            return r;
        }
        bool match(Parser * p, Location * begin) const
        {
            * begin = p->location();
            return cast().match(p);
        }
        const ConcreteNode & cast() const { return static_cast<const ConcreteNode &>(* this); }
    };
    struct Rule final : public ConcreteNodeBase<Rule>
    {
        using ConcreteNodeBase<Rule>::match;
        Rule(const char * name = nullptr)
          : name(name), _value(nullptr)
        { }
        template <typename ConcreteNode>
        Rule(const ConcreteNodeBase<ConcreteNode> & n)
          : name(nullptr), _value(nullptr)
        {
            operator=(n);
        }
        ~Rule() { free(); }
        void free()
        {
            // Only delete value if was allocated during assign
            auto r = dynamic_cast<const Rule *>(_value);
            if (r == nullptr)
                delete _value;
            _value = nullptr;
        }
        bool match(Parser * p) const override
        {
            if constexpr (ELL_DEBUG == 1)
            {
                if (this->name != nullptr)
                {
                    p->traceBegin(this);
                    return p->traceEnd(this, _value->match(p));
                }
            }
            return _value->match(p);
        }
        void describe(std::ostream & os) const override
        {
            if (this->name != nullptr)
            {
                os << this->name;
                return;
            }
            if (recursionFlag)
            {
                os << "...";
                return;
            }
            recursionFlag = true;
            _value->describe(os);
            recursionFlag = false;
        }
        template <typename ConcreteNode>
        void operator = (const ConcreteNodeBase<ConcreteNode> & n)
        {
            free(); // Why not reassigning a rule?
            auto r = dynamic_cast<const Rule *>(& n);
            if (r != nullptr)
            {
                _value = r;
                return;
            }
            _value = new ConcreteNode(n.cast());
        }
        Rule & setName(const char * name)
        { 
            this->name = name;
            return * this;
        }
        Rule & setTraceName(const char * name)
        { 
            if constexpr (ELL_DEBUG == 1)
                this->name = name;
            else
                this->name = nullptr;
            return * this;
        }
    private:
        const char * name;
        const NodeBase * _value;
        mutable bool recursionFlag = false;
        Rule(const Rule &);
        void operator = (const Rule &);
    };
    // Class used to handle rule children by ref and other nodes by value
    template <typename Child>
    struct NodeChild
    {
        NodeChild(const ConcreteNodeBase<Child> & n)
          : node(n.cast())
        { }
        Child node;
    };
    template <>
    struct NodeChild<Rule>
    {
        NodeChild(const ConcreteNodeBase<Rule> & n)
          : node(n.cast())
        { }
        const Rule & node;
    };
    template <typename Child>
    inline std::ostream & operator << (std::ostream & os, const NodeChild<Child> & n)
    {
        n.node.describe(os);
        return os;
    }
    // Abstract class for grammar unary nodes
    template <typename Child, typename ConcreteNode>
    struct UnaryNodeBase : public ConcreteNodeBase<ConcreteNode>
    {
    protected:
        UnaryNodeBase(const ConcreteNodeBase<Child> & child)
          : child(child.cast())
        { }
        NodeChild<Child> child;
    };
    // Implementation of + * ! BNF operators
    template <const int Min, const int Max, typename Child>
    struct RepeatNode final : public UnaryNodeBase<Child, RepeatNode<Min,Max,Child>>
    {
        using ConcreteNodeBase<RepeatNode<Min,Max,Child>>::match;
        RepeatNode(const ConcreteNodeBase<Child> & child)
          : UnaryNodeBase<Child,RepeatNode<Min,Max,Child>>(child)
        { }
        void describe(std::ostream & os) const override
        {
            if (Min == 0 && Max == 1)
                os << '!';
            else if (Min == 0 && Max == -1)
                os << '*';
            else if (Min == 1 && Max == -1)
                os << '+';
            else 
            {
                os << this->child << '{' << Min << ',' << Max << '}';
                return;
            }
            os << '(' << this->child << ')';
        } 
        bool match(Parser * p) const
        {
            p->traceBegin(this);
            Location begin = p->location();
            Location beforeSkipping = begin;
            int count = 0;
            while ((Max == -1 || count < Max) && this->child.node.match(p))
            {
                ++count;
                beforeSkipping = p->location();
                p->trySkip();
            }
            if (count >= Min)
            {
                p->setLocation(beforeSkipping);
                p->traceEnd(this, "=");
                return true;
            }
            p->setLocation(begin);
            p->traceEnd(this, "#");
            return false;
        }
    };
    // Repetition of a node a particular number of times which is given by a variable
    template <typename Child, typename IntType>
    struct DynRepeatNode final : public UnaryNodeBase<Child, DynRepeatNode<Child,IntType>>
    {
        using ConcreteNodeBase<DynRepeatNode<Child,IntType>>::match;
        DynRepeatNode(const ConcreteNodeBase<Child> & child, const IntType & times)
          : UnaryNodeBase<Child,DynRepeatNode<Child,IntType>>(child),
            _times(times)
        { }
        void describe(std::ostream & os) const override { os << '(' << this->child << "){" << _times << '}'; }
        bool match(Parser * p) const
        {
            p->traceBegin(this);
            Location begin = p->location();
            Location beforeSkipping = begin;
            IntType count = 0;
            IntType times = _times;
            while (count < times && this->child.node.match(p))
            {
                ++count;
                beforeSkipping = p->location();
                p->trySkip();
            }
            if (count == times)
            {
                p->setLocation(beforeSkipping);
                p->traceEnd(this, true);
                return true;
            }
            p->setLocation(begin);
            p->traceEnd(this, false);
            return false;
        }
    private:
        const IntType & _times;
    };
    template <typename Child>
    const RepeatNode<0,-1,Child> operator * (const ConcreteNodeBase<Child> & arg) { return RepeatNode<0,-1,Child>(arg); }
    template <typename Child>
    const RepeatNode<1,-1,Child> operator + (const ConcreteNodeBase<Child> & arg) { return RepeatNode<1,-1,Child>(arg); }
    template <typename Child>
    const RepeatNode<0,1,Child> operator ! (const ConcreteNodeBase<Child> & arg) { return RepeatNode<0,1,Child>(arg); }
    template <typename Child, typename IntType>
    const DynRepeatNode<Child, IntType> operator * (const ConcreteNodeBase<Child> & arg, IntType & times) { return DynRepeatNode<Child, IntType>(arg, times); }
    template <typename Child, typename IntType>
    const DynRepeatNode<Child,IntType> operator * (const IntType & times, const ConcreteNodeBase<Child> & arg) { return DynRepeatNode<Child,IntType>(arg, times); }
    // Semantic action operator
    template <typename Lambda, typename Var, typename Child>
    struct ActionNode final : public UnaryNodeBase<Child, ActionNode<Lambda,Var,Child>>
    {
        using ConcreteNodeBase<ActionNode<Lambda,Var,Child>>::match;
        ActionNode(const ConcreteNodeBase<Child> & node, Lambda f)
          : UnaryNodeBase<Child, ActionNode<Lambda,Var,Child>>(node),
            _func(f)
        { }
        void dump(std::ostream & os) const override { os << "action(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }
        bool match(Parser * p, Var * v) const
        {
            if constexpr (std::is_same_v<bool,decltype(std::declval<Lambda>()(*v))>)
            {
                Location begin = p->location();
                if (! this->child.node.match(p, v))
                    return false;
                if (_func(*v))
                    return true;
                p->setLocation(begin);
                return false;
            }
            else if (this->child.node.match(p, v))
            {
                _func(*v);
                return true;
            }
            else return false;
        }
        bool match(Parser * p) const
        {
            Var v = Var();
            return match(p, & v);
        }
    private:
        Lambda _func;
    };
    // Specialization for an action which does not take any parameter.
    template <typename Lambda, typename Child>
    struct ActionNode<Lambda,void,Child> final : public UnaryNodeBase<Child, ActionNode<Lambda,void,Child>>
    {
        using ConcreteNodeBase<ActionNode<Lambda,void,Child>>::match;
        ActionNode(const ConcreteNodeBase<Child> & node, Lambda func)
          : UnaryNodeBase<Child, ActionNode<Lambda,void,Child>>(node),
            _func(func)
        { }
        void dump(std::ostream & os) const override { os << "action(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }
        bool match(Parser * p) const
        {
            if constexpr (std::is_same_v<bool,decltype(std::declval<Lambda>()())>)
            {
                Location begin = p->location();
                if (! this->child.node.match(p))
                    return false;
                if (_func())
                    return true;
                p->setLocation(begin);
                return false;
            }
            else if (this->child.node.match(p))
            {
                _func();
                return true;
            }
            else return false;
        }
    private:
        Lambda _func;
    };
    // Implementation of ConcreteNode::action now that ActionNode is defined
    template <typename ConcreteNode>
    template <typename Var, typename Lambda>
    const ActionNode<Lambda,Var,ConcreteNode> ConcreteNodeBase<ConcreteNode>::action(Lambda func) const
    {
        return ActionNode<Lambda, Var, ConcreteNode>(*this, func);
    }
    template <typename Var, typename Child>
    struct AssignNode final : public UnaryNodeBase<Child,AssignNode<Var,Child>>
    {
        using ConcreteNodeBase<AssignNode<Var,Child>>::match;
        AssignNode(const ConcreteNodeBase<Child> & node, Var & var)
          : UnaryNodeBase<Child, AssignNode<Var,Child>>(node),
            _var(var)
        { }
        void dump(std::ostream & os) const override { os << "assign(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }
        bool match(Parser * p, Var * v) const
        {
            bool r = this->child.node.match(p, v);
            if (r)
                _var = *v;
            return r;
        }
        bool match(Parser * p) const { return match(p, & _var); }
    private:
        Var & _var;
    };
    template <typename ConcreteNode>
    template <typename Var>
    const AssignNode<Var,ConcreteNode> ConcreteNodeBase<ConcreteNode>::operator [] (Var & var) const
    {
        return AssignNode<Var, ConcreteNode>(*this, var);
    }
    // Skipper disabling
    template <typename Child>
    struct NoSkipNode final : public UnaryNodeBase<Child,NoSkipNode<Child>>
    {
        using ConcreteNodeBase<NoSkipNode<Child>>::match;
        NoSkipNode(const ConcreteNodeBase<Child> & child)
          : UnaryNodeBase<Child,NoSkipNode<Child>>(child)
        { }
        void dump(std::ostream & os) const override { os << "noskip(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }
        bool match(Parser * p) const
        {
            SafeModify<const NodeBase*> ms(p->_skipper, nullptr);
            // Disable trace inside terminals
            SafeModify<bool> mt(p->_showTrace, false);
            return this->child.node.match(p);
        }
    };
    // Disables error when second operand of aggregation does not match
    template <typename Child>
    struct FallbackNode final : public UnaryNodeBase<Child,FallbackNode<Child>>
    {
        using ConcreteNodeBase<FallbackNode<Child>>::match;
        FallbackNode(const ConcreteNodeBase<Child> & child)
          : UnaryNodeBase<Child,FallbackNode<Child>>(child)
        { }
        void dump(std::ostream & os) const override { os << "fallback(" << this->child << ')'; }
        void describe(std::ostream & os) const override { os << this->child; }
        bool match(Parser * p) const
        {
            p->traceBegin(this);
            SafeModify<bool> ms(p->_fallback, true);
            return p->traceEnd(this, this->child.node.match(p));
        }
    };
    // Match without consuming any character
    template <typename Child>
    struct NoConsumeNode final : public UnaryNodeBase<Child,NoConsumeNode<Child>>
    {
        using ConcreteNodeBase<NoConsumeNode<Child>>::match;
        NoConsumeNode(const ConcreteNodeBase<Child> & child)
          : UnaryNodeBase<Child,NoConsumeNode<Child>>(child)
        { }
        void describe(std::ostream & os) const override { os << "check(" << this->child << ")"; }
        bool match(Parser * p) const
        {
            p->traceBegin(this);
            Location begin = p->location();
            bool r = this->child.node.match(p);
            p->setLocation(begin);
            return p->traceEnd(this, r);
        }
    };
    // Abstract class for grammar binary nodes
    template <typename ConcreteNode, typename Left, typename Right>
    struct BinaryNodeBase : public ConcreteNodeBase<ConcreteNode>
    {
        NodeChild<Left> left;
        NodeChild<Right> right;
    protected:
        BinaryNodeBase(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : left(left.cast()), right(right.cast())
        { }
    };
    // Operator >>
    template <typename Left, typename Right>
    struct SequenceNode final : public BinaryNodeBase<SequenceNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<SequenceNode<Left,Right>>::match;
        SequenceNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<SequenceNode<Left,Right>,Left,Right>(left, right)
        { }
        void dump(std::ostream & os) const override { os << this->left << ' ' << this->right; }
        void describe(std::ostream & os) const override { os << this->left; }
        bool match(Parser * p) const
        {
            Location begin = p->location();
            if (this->left.node.match(p))
            {
                p->trySkip();
                if (this->right.node.match(p))
                    return true;
                if (! p->_fallback)
                    p->raise_error(& this->right.node);
            }
            p->setLocation(begin);
            return false;
        }
    };
    template <typename Left, typename Right>
    const SequenceNode<Left, Right> operator >> (const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
    {
        return SequenceNode<Left, Right>(left, right);
    }
    // Operator %
    template <typename Left, typename Right>
    struct ListNode final : public BinaryNodeBase<ListNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<ListNode<Left,Right>>::match;
        ListNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<ListNode<Left,Right>,Left,Right>(left, right)
        { }
        void describe(std::ostream & os) const override { os << this->left << " separated by " << this->right; }
        bool match(Parser * p) const
        {
            bool matched = false;
            Location begin = p->location();
            while (this->left.node.match(p))
            {
                matched = true;
                begin = p->location();
                p->trySkip();
                if (! this->right.node.match(p))
                    break;
                p->trySkip();
            }
            p->setLocation(begin);
            return matched;
        }
    };
    template <typename Left, typename Right>
    const ListNode<Left, Right> operator % (const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
    {
        return ListNode<Left, Right>(left, right);
    }
    // Operator |
    template <typename Left, typename Right>
    struct ChoiceNode final : public BinaryNodeBase<ChoiceNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<ChoiceNode<Left,Right>>::match;
        void describe(std::ostream & os) const override { os << this->left << " or " << this->right; }
        ChoiceNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<ChoiceNode<Left,Right>,Left,Right>(left, right)
        { }
        bool match(Parser * p) const
        {
            return this->left.node.match(p) || this->right.node.match(p);
        }
    };
    template <typename Left, typename Right>
    const ChoiceNode<Left,Right> operator | (const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
    {
        return ChoiceNode<Left, Right>(left, right);
    }
    // Checks that a node is not followed by another one without consuming it, and without skipping
    // Used to build identifiers rules. Ex: keyword = nosfx(str("keyword"), chset("a-zA-Z_0-9"))
    template <typename Left, typename Right>
    struct NoSuffixNode final : public BinaryNodeBase<NoSuffixNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<NoSuffixNode<Left,Right>>::match;
        NoSuffixNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<NoSuffixNode<Left,Right>,Left,Right>(left, right)
        { }
        void describe(std::ostream & os) const override { os << this->left; }
        bool match(Parser * p) const
        {
            Location begin = p->location();
            if (! this->left.node.match(p))
                return false;
            // No skip in here
            if (! this->right.node.match(p))
                return true;
            p->setLocation(begin);
            return false;
        }
    };
    // Exclusion operator (-)
    template <typename Left, typename Right>
    struct ExclusionNode final : public BinaryNodeBase<ExclusionNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<ExclusionNode<Left,Right>>::match;
        ExclusionNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<ExclusionNode<Left,Right>,Left,Right>(left, right)
        { }
        void describe(std::ostream & os) const override { os << this->left << " except " << this->right; }
        bool match(Parser * p) const
        {
            p->traceBegin(this);
            Location begin = p->location();
            if (this->right.node.match(p))
            {
                p->setLocation(begin);
                return p->traceEnd(this, false);
            }
            return p->traceEnd(this, this->left.node.match(p));
        }
    };
    template <typename Left, typename Right>
    const ExclusionNode<Left,Right> operator - (const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
    {
        return ExclusionNode<Left, Right>(left, right);
    }
    // Skipper setting
    template <typename Left, typename Right>
    struct SkipNode final : public BinaryNodeBase<SkipNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<SkipNode<Left,Right>>::match;
        SkipNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<SkipNode<Left,Right>,Left,Right>(left, right)
        { }
        void dump(std::ostream & os) const override { os << "skip(" << this->left << ", " << this->right << ")"; }
        void describe(std::ostream & os) const override { os << this->right; }
        bool match(Parser * p) const
        {
            p->traceBegin(this);
            SafeModify<const NodeBase*> ms(p->_skipper, & this->left.node);
            p->trySkip();
            return p->traceEnd(this, this->right.node.match(p));
        }
    };
    // Set of characters, use minus to specify a range (ex: -a-zA-Z_0-9)
    struct CharSetNode final : public ConcreteNodeBase<CharSetNode>
    {
        using ConcreteNodeBase<CharSetNode>::match;
        CharSetNode(const char * set)
          : _set(set)
        { }
        bool match(Parser * p) const override
        {
            char c;
            return match(p, & c);
        }
        bool match(Parser * parser, char * out) const
        {
            const char * p = & _set[0];
            const char c = *parser->position();
            while (* p)
            {
                const char first = * p;
                if (c == first)
                {
                    *out = c;
                    parser->next();
                    parser->trace(this, "=");
                    return true;
                }
                ++p;
                if (* p == '-' && * (p + 1))
                {
                    const char second = * (p + 1);
                    if ((c >= first) & (c <= second))
                    {
                        *out = c;
                        parser->next();
                        parser->trace(this, "=");
                        return true;
                    }
                    p += 2;
                }
            }
            parser->trace(this, "#");
            return false;
        }
        void describe(std::ostream & os) const override
        {
            os << '[';
            dump_string(os, _set);
            os << ']';
        }
    private:
        const char* _set;
    };
    // Match a particular character
    struct CharNode final : public ConcreteNodeBase<CharNode>
    {
        using ConcreteNodeBase<CharNode>::match;
        CharNode(char c)
          : _c(c)
        { }
        bool match(Parser * p) const override
        {
            char c;
            return match(p, & c);
        }
        bool match(Parser * parser, char * out) const
        {
            if (* parser->position() == _c)
            {
                *out = _c;
                parser->next();
                parser->trace(this, "=");
                return true;
            }
            parser->trace(this, "#");
            return false;
        }
        void describe(std::ostream & os) const override
        {
            os << '\'';
            dump_char(os, _c);
            os << '\'';
        }
    private:
        char _c;
    };
    // Match an inclusive range of characters
    struct RangeNode final : public ConcreteNodeBase<RangeNode>
    {
        using ConcreteNodeBase<RangeNode>::match;
        RangeNode(char from, char to)
          : _from(from), _to(to)
        { }
        bool match(Parser * p) const override
        {
            char c;
            return match(p, & c);
        }
        bool match(Parser * p, char * out) const
        {
            const char c = * p->position();
            if (c >= _from && c <= _to)
            {
                *out = c;
                p->next();
                p->trace(this, "=");
                return true;
            }
            p->trace(this, "#");
            return false;
        }
        void describe(std::ostream & os) const override
        {
            os << '[';
            dump_char(os, _from);
            os << '-';
            dump_char(os, _to);
            os << ']';
        }
    private:
        char _from, _to;
    };
    // Matches a string
    struct StrNode final : public ConcreteNodeBase<StrNode>
    {
        using ConcreteNodeBase<StrNode>::match;
        StrNode(const char * s)
          : _string(s)
        { 
            if constexpr (ELL_DEBUG == 1)
            {
                if (_string.find('\n') != _string.npos)
                    throw Error("New line character in string primitive not supported");
            }
        }
        bool match(Parser * parser) const override
        {
            if (parser->remaining() < (int)_string.size() ||
                memcmp(_string.c_str(), parser->position(), _string.size()) != 0)
            {
                parser->trace(this, "#");
                return false;
            }
            parser->sameLineNext(_string.size());
            parser->trace(this, "=");
            return true;
        }
        void describe(std::ostream & os) const override
        {
            os << '"';
            dump_string(os, _string);
            os << '"';
        }
    private:
        std::string _string;
    };
    // Matches a string, ignoring case
    struct IgnStrNode final : public ConcreteNodeBase<IgnStrNode>
    {
        using ConcreteNodeBase<IgnStrNode>::match;
        static char to_lower(char c) { return ((c >= 'A') & (c <= 'Z')) ? (c - 'A' + 'a') : c; }
        IgnStrNode(const char * s)
          : _string(s)
        {
            if constexpr (ELL_DEBUG == 1)
            {
                if (_string.find('\n') != _string.npos)
                    throw Error("New line character in string primitive not supported");
            }
            std::transform(_string.begin(), _string.end(), _string.begin(), to_lower);
        }
        bool match(Parser * parser) const override
        {
            int n = _string.size();
            if (parser->remaining() < n)
            {
                parser->trace(this, "#");
                return false;
            }
            Location begin = parser->location();
            for (int i = 0; i < n; ++i)
            {
                if (to_lower(parser->position()[i]) != _string[i])
                {
                    parser->setLocation(begin);
                    parser->trace(this, "#");
                    return false;
                }
            }
            parser->sameLineNext(n);
            parser->trace(this, "=");
            return true;
        }
        void describe(std::ostream & os) const override
        {
            os << "i\"";
            dump_string(os, _string);
            os << '"';
        }
    private:
        std::string _string;
    };
    // Matches end of the string being parsed
    struct EosNode final : public ConcreteNodeBase<EosNode>
    {
        using ConcreteNodeBase<EosNode>::match;
        void describe(std::ostream & os) const override { os << "end"; }
        bool match(Parser * p) const override
        {
            bool matched = p->remaining() <= 0;
            p->trace(this, matched ? "=" : "#");
            return matched;
        }
    };
    // Matches nothing, usefull to apply a default semantic action on an branch
    struct EpsNode final : public ConcreteNodeBase<EpsNode>
    {
        using ConcreteNodeBase<EpsNode>::match;
        void describe(std::ostream & os) const override { os << "eps"; }
        bool match(Parser * p) const override
        {
            p->trace(this, "=");
            return true;
        }
    };
    // Matches any character
    struct AnyNode final : public ConcreteNodeBase<AnyNode>
    {
        using ConcreteNodeBase<AnyNode>::match;
        void describe(std::ostream & os) const override { os << "any"; }
        bool match(Parser * p) const override
        {
            char c;
            return match(p, & c);
        }
        bool match(Parser * p, char * out) const
        {
            if (p->remaining() == 0)
            {
                p->trace(this, "#");
                return false;
            }
            * out = * p->position();
            p->next();
            p->trace(this, "=");
            return true;
        }
    };
    // Never matches
    struct NopeNode final : public ConcreteNodeBase<NopeNode>
    {
        using ConcreteNodeBase<NopeNode>::match;
        void describe(std::ostream & os) const override { os << "nop"; }
        bool match(Parser * p) const override
        {
            p->trace(this, "#");
            return false;
        }
    };
    // Matches a real constant
    struct RealNode final : public ConcreteNodeBase<RealNode>
    {
        using ConcreteNodeBase<RealNode>::match;
        void describe(std::ostream & os) const override { os << "real"; }
        bool match(Parser * p) const override
        {
            double d;
            return match(p, & d);
        }
        bool match(Parser * p, double * out) const
        {
            char * end;
            // from_chars not suported for double at the moment
            // end = std::from_chars(p->position(), p->end(), *out).ptr;
            * out = strtod(p->position(), & end);
            if (end == p->position())
            {
                p->trace(this, "#");
                return false;
            }
            p->sameLineNext(end - p->position());
            p->trace(this, "=");
            return true;
        }
    };
    // Matches an integer constant
    template <int Base=10, int MinChars=1, int MaxChars=-1, typename IntType=int>
    struct IntegerNode final : public ConcreteNodeBase<IntegerNode<Base,MinChars,MaxChars,IntType>>
    {
        using ConcreteNodeBase<IntegerNode<Base,MinChars,MaxChars,IntType>>::match;
        void describe(std::ostream & os) const override
        {
            if constexpr (Base == 16)
                os << "hexadecimal";
            else if constexpr (Base == 8)
                os << "octal";
            else
                os << "integer";
        }
        bool match(Parser * p) const override
        {
            IntType d;
            return match(p, & d);
        }
        bool match(Parser * p, IntType * out) const
        {
            const char * end;
            if constexpr (MaxChars == -1)
                end = p->end();
            else
                end = std::min(p->position() + MaxChars, p->end());
            end = std::from_chars(p->position(), end, *out, Base).ptr;
            int consumed = end - p->position();
            if (consumed < MinChars)
            {
                p->trace(this, "#");
                return false;
            }
            p->sameLineNext(consumed);
            p->trace(this, "=");
            return true;
        }
    };
    // Raise an error with custom message
    struct ErrorNode final : public ConcreteNodeBase<ErrorNode>
    {
        using ConcreteNodeBase<ErrorNode>::match;
        ErrorNode(const std::string & msg)
          : _msg(msg)
        { }
        bool match(Parser * p) const override
        {
            p->raise_error(nullptr, _msg.c_str());
            return false;
        }
        void describe(std::ostream & os) const override { os << "error(\"" << _msg << "\")"; }
    private:
        std::string _msg;
    };
    // Any ell grammar should extends this class.
    struct Grammar
    {
        // Binary operators
        // (also see operators `>>`, `|` and `-`.)
        template <typename Left, typename Right> using nosfx = NoSuffixNode<Left, Right>;
        template <typename Left, typename Right> using skip = SkipNode<Left, Right>;
        // Unary operators
        // (also see operators `+`, `*`, `!`, `[]` operators and `action` method.)
        template <typename Child> using noskip = NoSkipNode<Child>;
        template <typename Child> using fallback = FallbackNode<Child>;
        template <typename Child> using check = NoConsumeNode<Child>;
        template <int Min, int Max, typename Child>
        const RepeatNode<Min, Max, Child> repeat(const ConcreteNodeBase<Child> & arg) { return RepeatNode<Min, Max, Child>(arg); }
        // Short-hand to express a lexeme in the grammar (ie. making a new primitive)
        template <typename Child>
        const FallbackNode<NoSkipNode<Child>> token(const ConcreteNodeBase<Child> & child)
        {
            return fallback(noskip(child));
        }
        // Primitives
        using chset = CharSetNode;
        using ch = CharNode;
        using range = RangeNode;
        using str = StrNode;
        using istr = IgnStrNode;
        using error = ErrorNode;
        template <int Base=10, int MinChars=1, int MaxChars=-1, typename IntType=int>
        using integer = IntegerNode<Base,MinChars,MaxChars,IntType>;
        const IntegerNode<> dec;
        const IntegerNode<10,1,-1,unsigned int> udec;
        const RealNode real;
        const EosNode eos;
        const EpsNode eps;
        const NopeNode nop;
        const AnyNode any;
    };
}
