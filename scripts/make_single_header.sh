#!/bin/sh
echo "/*"
cat COPYING
echo "*/"
echo "#pragma once"
grep -h "^#include <" ell/* | sort -u
echo "#ifndef ELL_DEBUG"
echo "#define ELL_DEBUG 1"
echo "#endif"
echo "namespace ell"
echo "{"
for h in Error Utils Location NodeBase Parser ConcreteNodeBase Rule NodeChild UnaryNodes BinaryNodes PrimitiveNodes Grammar
do
    file="ell/$h.h"
    if [ ! -e "$file" ]; then
        echo "File $file does not exist" 1>&2
        exit 1
    fi
    cat $file | grep -v "^$" | grep -v "^#" | grep -v "^namespace ell" | grep -v "^{" | grep -v "^}"
done
echo "}"
